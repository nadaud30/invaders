/***************************************
*									   *
* JS - Projet Invaders - Nadaud Alexis *
*									   *
***************************************/

/* Objet basique */
var UFO = function(x, y, largeur, hauteur, vitesse, estDetruit) {
    this.positionX = x;
    this.positionY = y;
    this.largeur = largeur;
    this.hauteur = hauteur;
    this.vitesse = vitesse;
    this.estDetruit = estDetruit;
}

/*********************/
/* Getters & Setters */
/*********************/

/* Position X (top) */
UFO.prototype.getPositionX = function() {
    return this.positionX;
}
UFO.prototype.setPositionX = function(x) {
    this.positionX = x;
}

/* Position Y (left) */
UFO.prototype.getPositionY = function() {
    return this.positionY;
}
UFO.prototype.setPositionY = function(y) {
    this.positionY = y;
}

/* Largeur */
UFO.prototype.getLargeur = function() {
    return this.largeur;
}
UFO.prototype.setLargeur = function(largeur) {
    this.largeur = largeur;
}

/* Hauteur */
UFO.prototype.getHauteur = function() {
    return this.hauteur;
}
UFO.prototype.setHauteur = function(hauteur) {
    this.hauteur = hauteur;
}

/* Vitesse */
UFO.prototype.getVitesse = function() {
    return this.vitesse;
}
UFO.prototype.setVitesse = function(vitesse) {
    this.vitesse = vitesse;
}

/* Statut */
UFO.prototype.getEstDetruit = function() {
    return this.estDetruit;
}
UFO.prototype.setEstDetruit = function(estDetruit) {
    this.estDetruit = estDetruit;
}

/* Se déplacer à gauche */
UFO.prototype.deplacerGauche = function() {
    this.positionX -= this.vitesse;
}

/* Se déplacer à droite */
UFO.prototype.deplacerDroite = function() {
    this.positionX += this.vitesse;
}

/* Se déplacer en haut */
UFO.prototype.deplacerHaut = function() {
    this.positionY += this.vitesse;
}

/* Se déplacer en bas */
UFO.prototype.deplacerBas = function() {
    this.positionY -= this.vitesse;
}


/****************/
/*   Vaisseau   */
/****************/

var Vaisseau = function(x, y, largeur, hauteur, estDetruit){
    UFO.call(this, x, y, largeur, hauteur, estDetruit);
}

Vaisseau.prototype = Object.create(UFO.prototype);
Vaisseau.prototype.constructor=Vaisseau;

/*************/
/*   Alien   */
/*************/

var Alien = function(x, y, largeur, hauteur, estDetruit){
    UFO.call(this, x, y, largeur, hauteur, estDetruit);
}

Alien.prototype = Object.create(UFO.prototype);
Alien.prototype.constructor=Alien;

/***************/
/*   Missile   */
/***************/

var Missile = function(x, y, largeur, hauteur, estDetruit){
    UFO.call(this, x, y, largeur, hauteur, estDetruit);
}

Missile.prototype = Object.create(UFO.prototype);
Missile.prototype.constructor=Missile;

