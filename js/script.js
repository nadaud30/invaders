/***************************************
*									   *
* JS - Projet Invaders - Nadaud Alexis *
*									   *
***************************************/
var Game = function(){

	var Model = {
		aliens:[],
		missiles:[],
		vaisseau:new Vaisseau(50,0,125,170,5,false),
		dernierAlien:null,
		x:0,
		y:700,
		
		creerAlien(){
			if(this.dernierAlien){
				this.x = this.dernierAlien.getPositionX() + 200;
				
				if(this.x + this.dernierAlien.getLargeur() > 1024){
					this.x = 0;
					this.y-=60;
				}
			}
			var alien = new Alien(this.x,this.y,150,50, 0.2, false);
			
			this.dernierAlien = alien;
			this.aliens.push(alien);
		},

		creerMissile(){
			var missile = new Missile(this.vaisseau.getPositionX()+50,170,25,100, 5, false);
			this.missiles.push(missile);
		},

		isCollisionUfo(ufo1, ufo2){
			return ufo1.getPositionX() < ufo2.getPositionX() + ufo2.getLargeur() &&
			ufo1.getPositionX() + ufo1.getLargeur() > ufo2.getPositionX() &&
			ufo1.getPositionY() < ufo2.getPositionY() + ufo2.getHauteur() &&
			ufo1.getHauteur() + ufo1.getPositionY() > ufo2.getPositionY()
		  },

	}

	var View = {
		gamediv: null,
		gamedivHeight: 780,
		gamedivWidth: 1024,

		creerDiv(){
			var div = document.createElement('div');
			div.style.height = View.gamedivHeight+"px";
			div.style.width = View.gamedivWidth+"px";
			div.style.backgroundImage = "url('./img/background/background.jpg')";
			div.style.borderRadius = "5px";
			div.style.margin = "auto";
			div.style.position = "relative";
			div.id = "game";
			div.style.overflow = "hidden";
			document.body.append(div);
			View.gamediv = div;
		},

		reset(){
			View.gamediv.innerHTML = "";
		},
		
		afficherUfo(ufo) {
			var ufoHtml = View.getTemplateByUfo(ufo);

			ufoHtml.style.left = ufo.getPositionX()+ "px";
			ufoHtml.style.bottom = ufo.getPositionY()+ "px";
			ufoHtml.style.width = ufo.getLargeur() + "px";
			ufoHtml.style.height = ufo.getHauteur() + "px";

			ufoHtml.style.position = "absolute";

			View.gamediv.appendChild(ufoHtml);
		},

		getTemplateByUfo(ufo){
			let selecteur;
			switch(ufo.constructor.name){
				case "Vaisseau" : selecteur = "vaisseau";
				break;
				case "Missile" : selecteur = "missile";
				break;
				case "Alien" : selecteur = "alien";
				break;
			}

			var temp = document.getElementById(selecteur);
			var clone = temp.content.cloneNode(true).firstElementChild;
			return clone;
		}
	}

	var Controller = {
		isKeyLeftDown: false,
		isKeyRightDown: false,
		isKeySpaceDown: false,
		isGameRunning: true,
		animer(){
			if(Controller.isGameRunning){
				requestAnimationFrame(Controller.animer);
				View.reset();
				Controller.render();
			}
			else {
				if(confirm('Game Over')) {
					Model.aliens = [];
					Model.x = 0;
					Model.y = 700;
					Model.vaisseau = new Vaisseau(50,0,125,170,5,false);
					Controller.isKeyRightDown = false;
					Controller.isKeyLeftDown = false;
					Controller.isKeySpaceDown = false;
					for(let i=0; i<20; i++){
						Model.creerAlien();
					}
					requestAnimationFrame(Controller.animer);
					Controller.isGameRunning = true;
				}
			}
		},

		render(){
			View.afficherUfo(Model.vaisseau);
			if(Controller.isKeyLeftDown && (Model.vaisseau.getPositionX() - Model.vaisseau.getVitesse() >= 0)) {
				Model.vaisseau.deplacerGauche();
			}
			if(Controller.isKeyRightDown && (Model.vaisseau.getPositionX() + Model.vaisseau.getLargeur() + Model.vaisseau.getVitesse() < View.gamedivWidth)) {
				Model.vaisseau.deplacerDroite();
			}

			Model.aliens.forEach(alien => {
				View.afficherUfo(alien);
				alien.deplacerBas();
				if(Model.isCollisionUfo(alien, Model.vaisseau)){
					Controller.isGameRunning = false;
				}
			});
			Model.missiles.forEach(missile => {
				View.afficherUfo(missile);
		
				if(missile.getPositionY() + missile.getVitesse() < View.gamedivHeight){
					missile.deplacerHaut();
				}
			
				else {
					Model.missiles.splice(Model.missiles.indexOf(missile), 1);			
				}

				Model.aliens.forEach(alien => {
					if(Model.isCollisionUfo(alien, missile)){
						Model.aliens.splice(Model.aliens.indexOf(alien), 1);	
						Model.missiles.splice(Model.missiles.indexOf(missile), 1);	
					}
				});
			});
			
		},

		initListeners(){
			document.addEventListener("keydown", (event) => {
				if(event.key == "ArrowLeft"){
					Controller.isKeyLeftDown = true;
				}
				else if(event.key == "ArrowRight"){
					Controller.isKeyRightDown = true;
				}
				else if(event.key == " " && Controller.isKeySpaceDown == false){
					Model.creerMissile();
					Controller.isKeySpaceDown = true;
				}
			});

			document.addEventListener("keyup", (event) => {
				if(event.key == "ArrowLeft"){
					Controller.isKeyLeftDown = false;
				}
				else if(event.key == "ArrowRight"){
					Controller.isKeyRightDown = false;
				}
				else if(event.key == " "){
					Controller.isKeySpaceDown = false;
				}
			});
		}
	}


 

	return{
		start: function(){
			View.creerDiv();
			Controller.initListeners();

			for(let i=0; i<20; i++){
				Model.creerAlien();
			}
			Controller.animer();
		}
	}
}();

Game.start();
